This program starts with a blank canvas and iteratively draws an image using shapes with random orientation, size, and color.

The process is as follows:

1. An image is provided and a blank canvas is created.
2. A random pixel is chosen from the source image and its color is stored.
3. A shape with a random orientation, size, and position is chosen.
4. Check if the canvas would be closer to the source image if the shape were to be drawn onto it.
5. If it is, draw the shape onto the canvas.
6. Otherwise discard the shape.
7. Repeat from step 2 for some number of iterations.

Dependencies
------------

* SDL2
* glfw
* gl3w
