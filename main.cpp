#include <cassert>
#include <cstdint>
#include <cstdbool>
#include <cstdio>
#include <ctime>

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include <SDL2/SDL.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

#include "jint.h"

// NOTE: Program is blindly copying the pixels, not taking mem orientation
//       or anything into account (might be fully handled by SDL).
//       Works so far.

#define min(x, y) ((x) < (y) ? (x) : (y))
#define max(x, y) ((x) > (y) ? (x) : (y))

enum class RenderLib {
	NONE,
	OPENGL,
	SDL,
} renderlib = RenderLib::OPENGL;

enum class Shape {
	NOSHAPE,
	SQUARE,
	TRIANGLE,
	LINE,
	CIRCLE,
};

struct Args {
	char *fileName;
	u64 maxIter;
	Shape shape;
	u16 blitSize;
	bool video;
};

struct Image {
	u8 *data;
	int w;
	int h;
	int bpp;
};

struct Color {
	u32 r;
	u32 g;
	u32 b;
};

struct Point {
	s32 x;
	s32 y;
};

GLFWwindow *glwindow = nullptr;
SDL_Window *sdlwindow = nullptr;
SDL_Surface *sdlwindowSurface = nullptr;
SDL_Renderer *sdlrenderer = nullptr;

auto vertShdr =
"#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"layout (location = 1) in vec2 aTexCoord;\n"
"\n"
"out vec2 TexCoord;\n"
"\n"
"void main()\n"
"{\n"
"	gl_Position = vec4(aPos, 1.0);\n"
"	TexCoord = aTexCoord;\n"
"}\n";

auto fragShdr =
"#version 330 core\n"
"out vec4 FragColor;\n"
"\n"
"in vec2 TexCoord;\n"
"\n"
"uniform sampler2D ourTexture;\n"
"\n"
"void main()\n"
"{\n"
"	FragColor = texture(ourTexture, TexCoord);\n"
"}\n";

bool windowShouldClose = false;

/* void get_average_color(Color *color, SDL_Surface *image, Vec *vector, Shape shape); */

auto render_sdl(Image img)
{
	auto rmask = 0x000000ff;
	auto gmask = 0x0000ff00;
	auto bmask = 0x00ff0000;
	auto amask = 0x00000000;
	auto depth = 24;
	auto pitch = img.bpp * img.w;
	auto surface = SDL_CreateRGBSurfaceFrom((void *) img.data, img.w, img.h,
					    depth, pitch, rmask, gmask, bmask, amask);
	assert(surface);

	// non gpu way
	if (SDL_BlitSurface(surface, 0, sdlwindowSurface, 0)) {
		puts("error blitting");
		return;
	}
	SDL_UpdateWindowSurface(sdlwindow);
	SDL_FreeSurface(surface);

	// gpu way - slower probably because it creates new texture every frame
	/* SDL_Texture *texture = SDL_CreateTextureFromSurface(sdlrenderer, sdlwindowSurface); */
	/* SDL_RenderCopy(sdlrenderer, texture, 0, 0); */
	/* SDL_DestroyTexture(texture); */
	/* SDL_RenderPresent(sdlrenderer); */
}

auto render_opengl(Image img)
{
	img.w = img.w;
	glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	// generate texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.w, img.h, 0, GL_RGB, GL_UNSIGNED_BYTE, img.data);
	glGenerateMipmap(GL_TEXTURE_2D);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glfwSwapBuffers(glwindow);
	glfwPollEvents();
}

auto render(Image img)
{
	switch (renderlib) {
	case RenderLib::SDL:
		render_sdl(img);
		break;
	case RenderLib::OPENGL:
		render_opengl(img);
		break;
	default:
		printf("E: no rendering library selected");
		exit(1);
		break;
	}
}

auto save_to_ppm(Image img, char *name)
{
	auto file = fopen(name, "wb");
	assert(file);

	fprintf(file, "P6\n%d %d\n255\n", img.w, img.h);
	fwrite(img.data, img.bpp, img.w * img.h, file);
	fclose(file);

	return true;
}

auto get_rand_pixel_point(Image img) -> Point
{
	auto x = rand() % img.w;
	auto y = rand() % img.h;

	return Point { x, y };
}

auto get_color(Point point, Image img) -> Color
{
	auto color = Color {};
	auto pos = point.y * img.w + point.x;
	assert(pos < img.w * img.h);
	auto bytePos = pos * 3;

	auto pixel = img.data + bytePos;
	color.r = pixel[0];
	color.g = pixel[1];
	color.b = pixel[2];

	return color;
}

auto get_color_average(Image img, Point pa, Point pb, Shape shape)
{
	auto r = uint {};
	auto g = uint {};
	auto b = uint {};
	auto count = uint {};

	if (shape == Shape::LINE) {
		auto slope = static_cast<float>(pb.y - pa.y) /
		             static_cast<float>(pb.x - pa.x);
		auto minPoint = pa.x < pb.x ? pa : pb;
		auto maxPoint = pa.x > pb.x ? pa : pb;
		for (auto x = minPoint.x; x < maxPoint.x; ++x) {
			auto y = minPoint.y + static_cast<int>((x - minPoint.x) * slope);
			if (y < 0 || y >= img.h) {
				continue;
			}
			auto color = get_color({ x, y }, img);
			r += color.r;
			g += color.g;
			b += color.b;
			++count;
		}
	} else if (shape == Shape::SQUARE) {
	}
	if (!count) { count = 1; }
	return Color { r / count, g / count, b / count };
}

auto handle_args(int argc, char **argv) -> Args
{
	auto blitSize = 20_u16;
	auto fileName = argv[argc - 1];
	auto maxIter = 10000000_u64;
	auto video = false;
	auto shape = Shape::LINE;

	// parse arguments
	for (auto i = 1; i < (argc - 1); ++i) {
		// make sure there is an argument between i and filename
		auto currArg = argv[i];
		if (currArg[0] == '-' && i < (argc - 2)) {
			switch (currArg[1]) {
			case 's':
				blitSize = atoi(argv[i + 1]);
				if (blitSize <= 0) {
					fprintf(stderr, "Error assigning blit size arg; defaulting to 3\n");
					blitSize = 3;
				}
				break;
			case 'i':
				maxIter = atoi(argv[i + 1]);
				if (maxIter == 0) {
					fprintf(stderr, "Error assigning iteration size arg; defaulting to 10,000,000\n");
					maxIter = 10000000;
				}
				break;
			case 'v':
				video = atoi(argv[i + 1]) > 0;
				break;
			default:
				fprintf(stderr, "Error: Invalid arguments\n");
				exit(1);
			}
			++i;
		} else {
			fprintf(stderr, "Error: Invalid arguments1\n");
			exit(1);
		}
	}

	auto args = Args {
		fileName,
			maxIter,
			shape,
			blitSize,
			video,
	};

	return args;
}

void
init(int w, int h, Args args)
{
	if (args.video) {
		if (renderlib == RenderLib::NONE) {
			printf("E: no renderlib selected");
			exit(1);
		} else if (renderlib == RenderLib::SDL) {
		// init required SDL systems
		SDL_Init(SDL_INIT_VIDEO);
		sdlwindow = SDL_CreateWindow("image magick", SDL_WINDOWPOS_UNDEFINED,
					     SDL_WINDOWPOS_UNDEFINED, w, h,
					     SDL_WINDOW_SHOWN);
		sdlwindowSurface = SDL_GetWindowSurface(sdlwindow);
		sdlrenderer = SDL_CreateRenderer(sdlwindow, -1, SDL_RENDERER_ACCELERATED);
		} else if (renderlib == RenderLib::OPENGL) {

			glfwInit();
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

			glwindow = glfwCreateWindow(w, h, "image magick", nullptr, nullptr);
			assert(glwindow);
			glfwMakeContextCurrent(glwindow);
			assert(!gl3wInit());
			glViewport(0, 0, w, h);

			float vertices[] = {
				1.0f,  1.0f,  0.0f,   1.0f,  1.0f,
				1.0f, -1.0f,  0.0f,   1.0f,  0.0f,
				-1.0f, -1.0f,  0.0f,  0.0f,  0.0f,
				-1.0f,  1.0f,  0.0f,  0.0f,  1.0f,
			};

			uint indices[] = {
				0, 1, 3,
				1, 2, 3,
			};

			uint vao;
			glGenVertexArrays(1, &vao);
			glBindVertexArray(vao);

			uint vbo;
			glGenBuffers(1, &vbo);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

			uint ebo;
			glGenBuffers(1, &ebo);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

			uint texture;
			glGenTextures(1, &texture);
			glBindTexture(GL_TEXTURE_2D, texture);

			// position
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *) 0);
			glEnableVertexAttribArray(0);

			// texture coords
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *) (3 * sizeof(float)));
			glEnableVertexAttribArray(1);

			//create shader
			auto vshader = glCreateShader(GL_VERTEX_SHADER);
			glShaderSource(vshader, 1, &vertShdr, nullptr);
			glCompileShader(vshader);
			int success;
			char infoLog[512];
			glGetShaderiv(vshader, GL_COMPILE_STATUS, &success);
			if (!success) {
				glGetShaderInfoLog(vshader, 512, nullptr, infoLog);
				puts("E:SHADER:VERT:COMPILE_FAIL");
				puts(infoLog);
				exit(1);
			}

			auto fshader = glCreateShader(GL_FRAGMENT_SHADER);
			glShaderSource(fshader, 1, &fragShdr, nullptr);
			glCompileShader(fshader);
			glGetShaderiv(fshader, GL_COMPILE_STATUS, &success);
			if (!success) {
				glGetShaderInfoLog(fshader, 512, nullptr, infoLog);
				puts("E:SHADER:FRAG:COMPILE_FAIL");
				puts(infoLog);
				exit(1);
			}

			auto shaderid = glCreateProgram();
			glAttachShader(shaderid, vshader);
			glAttachShader(shaderid, fshader);
			glLinkProgram(shaderid);
			glGetProgramiv(shaderid, GL_LINK_STATUS, &success);
			if (!success) {
				glGetProgramInfoLog(shaderid, 512, nullptr, infoLog);
				puts("E:SHADER:PROGRAM:LINK_FAIL");
				puts(infoLog);
				exit(1);
			}

			glDeleteShader(vshader);
			glDeleteShader(fshader);
			glUseProgram(shaderid);

			glUniform1i(glGetUniformLocation(shaderid, "ourTexture"), 0);
		}
	}
}

void
read_input()
{
	if (renderlib == RenderLib::NONE) {
		printf("E: no renderlib selected");
		exit(1);
	} else if (renderlib == RenderLib::SDL) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				windowShouldClose = true;
			}
		}
	} else if (renderlib == RenderLib::OPENGL) {
		if (glfwGetKey(glwindow, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
			glfwSetWindowShouldClose(glwindow, true);
		}

		if (glfwWindowShouldClose(glwindow)) {
			windowShouldClose = true;
		}
	} else {
		printf("E: no renderlib selected");
		exit(1);
	}
}

int main(int argc, char *argv[])
{
	if (argc == 1) {
		fprintf(stderr, "need a filename");
		return 0;
	}

	auto args = handle_args(argc, argv);

	auto srcImg = Image { 0, 0, 0, STBI_rgb };
	int n;
	if (renderlib == RenderLib::OPENGL) {
		stbi_set_flip_vertically_on_load(true);
	}
	srcImg.data = stbi_load(args.fileName, &srcImg.w, &srcImg.h, &n, srcImg.bpp);
	auto oldImg = srcImg;
	oldImg.data = reinterpret_cast<u8 *>(calloc(oldImg.bpp, oldImg.w * oldImg.h));

	if (!srcImg.data || !oldImg.data) {
		puts("error loading image");
		return 1;
	}

	init(srcImg.w, srcImg.h, args);

	auto currentIteration = 0_u64;
	auto blit = 0_u64;
	auto noblit = 0_u64;
	auto imageVersion = 0_u64;
	auto absTime = time(0);
	auto relTime = absTime;
	srand(absTime);

	auto startClock = clock();
	auto lastFrameClock = startClock;

	// Main loop
	while (!windowShouldClose) {
		if (args.video) read_input();
		// keep window open after finished if video, otherwise close
		if (currentIteration >= args.maxIter) {
			if (args.video) { render(oldImg); continue; } else { break; }
		}

		// Get new color from random pixel on src image
		auto rngPxPoint = get_rand_pixel_point(srcImg);
		auto rngPxColor = get_color(rngPxPoint, srcImg);

		// Get new random position to insert shape into
		auto checkPxPoint1 = get_rand_pixel_point(srcImg);
		auto checkPxPoint2 = Point {
			checkPxPoint1.x - args.blitSize + rand() % (2 * args.blitSize),
				checkPxPoint1.y - args.blitSize + rand() % (2 * args.blitSize),
		};
		if (checkPxPoint2.x >= oldImg.w) checkPxPoint2.x = oldImg.w - 1;
		if (checkPxPoint2.x < 0) checkPxPoint2.x = 0;
		if (checkPxPoint2.y >= oldImg.h) checkPxPoint2.y = oldImg.h - 1;
		if (checkPxPoint2.y < 0) checkPxPoint2.y = 0;
		auto srcPxColor = get_color_average(srcImg, checkPxPoint1, checkPxPoint2, args.shape);
		auto oldPxColor = get_color_average(oldImg, checkPxPoint1, checkPxPoint2, args.shape);
		/* get_color(&oldPxColor, &srcPxColor, oldImg, srcImg, blitSize, SQUARE, &vec); */

		// check if new image is better
		auto rngDiff = 0;
		auto oldDiff = 0;

		oldDiff += oldPxColor.r < srcPxColor.r ?
			srcPxColor.r - oldPxColor.r :
			oldPxColor.r - srcPxColor.r;
		oldDiff += oldPxColor.g < srcPxColor.g ?
			srcPxColor.g - oldPxColor.g :
			oldPxColor.g - srcPxColor.g;
		oldDiff += oldPxColor.b < srcPxColor.b ?
			srcPxColor.b - oldPxColor.b :
			oldPxColor.b - srcPxColor.b;

		rngDiff += rngPxColor.r < srcPxColor.r ?
			srcPxColor.r - rngPxColor.r :
			rngPxColor.r - srcPxColor.r;
		rngDiff += rngPxColor.g < srcPxColor.g ?
			srcPxColor.g - rngPxColor.g :
			rngPxColor.g - srcPxColor.g;
		rngDiff += rngPxColor.b < srcPxColor.b ?
			srcPxColor.b - rngPxColor.b :
			rngPxColor.b - srcPxColor.b;

		// do shit if improved
		if (oldDiff >= rngDiff) {
			auto color = rngPxColor;

			// copy the improved rectangle region
			if (args.shape == Shape::SQUARE) {
				for (auto y = 0; y < args.blitSize; ++y) {
					for (auto x = 0; x < args.blitSize; ++x) {
						auto currentX = checkPxPoint1.x + x;
						auto currentY = checkPxPoint1.y + y;
						if (currentX < 0 ||
						    currentX >= oldImg.w ||
						    currentY < 0 ||
						    currentY >= oldImg.h)
						{
							++noblit;
							continue;
						}
						auto pos = currentY * oldImg.w + currentX;
						auto bytePos = pos * oldImg.bpp;
						auto pixel = oldImg.data + bytePos;
						pixel[0] = color.r;
						pixel[1] = color.g;
						pixel[2] = color.b;
						++blit;
					}
				}
			} else if (args.shape == Shape::LINE) {
				// only checks every x, not every y. If y is larger it won't fill
				// all pixels.
				auto slope = static_cast<float>(checkPxPoint2.y - checkPxPoint1.y) /
					static_cast<float>(checkPxPoint2.x - checkPxPoint1.x);

				auto minPoint = checkPxPoint1.x < checkPxPoint2.x ? checkPxPoint1 : checkPxPoint2;
				auto maxPoint = checkPxPoint1.x > checkPxPoint2.x ? checkPxPoint1 : checkPxPoint2;
				for (auto x = minPoint.x; x < maxPoint.x; ++x) {
					auto y = minPoint.y + static_cast<int>((x - minPoint.x) * slope);
					if (y < 0 || y >= oldImg.h) {
						fprintf(stderr, "skipping:\n");
						fprintf(stderr, "minx:%d\n", minPoint.x);
						fprintf(stderr, "miny:%d\n", minPoint.y);
						fprintf(stderr, "x:%d\n", x);
						fprintf(stderr, "y:%d\n", y);
						fprintf(stderr, "slope:%f\n", slope);
						continue;
					}
					auto pos = y * oldImg.w + x;
					auto bytePos = pos * oldImg.bpp;
					auto pixel = oldImg.data + bytePos;
					pixel[0] = color.r;
					pixel[1] = color.g;
					pixel[2] = color.b;
				}
				++blit;
			}
		} else {
			++noblit;
		}

		++currentIteration;

		// save image / display to screen 100 times in total
		if (!(currentIteration % (args.maxIter / 100))) {
			auto currTime = time(0);
			printf("%llu\n", 100 * currentIteration / args.maxIter);
			printf("Relative time/1M: %llu\n", currTime - relTime);
			printf("Absolute time/1M: %llu\n", currTime - absTime);
			printf("Blits: %llu\n", blit);
			printf("Noblits: %llu\n", noblit);
			printf("Ratio: %.2lf\n", (double) blit / (double) noblit);
			printf("clocks since start: %ld\n\n", clock() - startClock);
			putchar('\n');
			blit = 0;
			noblit = 0;
			relTime = currTime;
			// display updated image

			if (!args.video) {
				char newName[256] = { 0 };
				sprintf(newName, "pics/%04llu.ppm", imageVersion++);

				if (!save_to_ppm(oldImg, newName)) {
					puts("error saving ppm");
					return 1;
				}
			}
		}


		// blit at a desired framerate if video
		if (args.video) {

			auto targetms = 1000.0 / 60;
			auto currFrameClock = clock();
			auto clocksForFrame = currFrameClock - lastFrameClock;
			auto currentms = 1000.0 * (double) clocksForFrame / CLOCKS_PER_SEC;
			if (currentms >= targetms) {
				render(oldImg);
				lastFrameClock = clock();
			}
		}
	}
	return 0;
}
