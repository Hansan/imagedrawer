CC = clang++
LIBS = -lglfw3 -lgl3w -lSDL2
BINDIR = bin
BIN = imgdrw
SRC = main.cpp
CFLAGS = -std=c++14 -Wall -Wextra -Wpedantic -Wshadow

ifeq ($(OS),Windows_NT)
	BIN := $(BIN).exe
	LIBS := -lmingw32 -lSDL2main $(LIBS) -lgdi32
endif

debug:
	${CC} ${CFLAGS} -g -o ${BINDIR}/${BIN} ${SRC} ${LIBS}

release:
	${CC} ${CFLAGS} -O2 -o ${BINDIR}/${BIN} ${SRC} ${LIBS}

test: debug
	${BINDIR}/${BIN}

clean:
	rm ${BINDIR}/${BIN}

syntax:
	${CC} ${CFLAGS} -fsyntax-only ${SRC}
